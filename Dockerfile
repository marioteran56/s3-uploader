# Node image
FROM node:slim
# Set app directory
WORKDIR /app
# Copy proyect files
COPY . .
# Install dependencies
RUN npm install
RUN npm install -g @angular/cli
# Google Analytics
ENV NG_CLI_ANALYTICS false
# Expose port 4200
EXPOSE 4200
# Run app
CMD ["ng", "serve", "--host", "0.0.0.0"]