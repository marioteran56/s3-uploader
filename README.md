# Amazon S3 Uploader

Este projecto consiste en un **subidor de archivos a Amazon S3**, el cual permite subir archivos de cualquier tipo dentro de un bucket de **S3**. El proyecto se encuentra desarrollado en **Angular 15**, donde se utliza el **SDK de AWS** para crear una conexión con el bucket de S3 y subir los archivos.

## Bucket de S3

### Creación de un bucket

Para poder subir archivos a S3, es necesario crear un bucket en la consola de AWS. Para crear un bucket, se debe ingresar a la consola de AWS y dirigirse a la sección de S3. Una vez dentro de S3, se debe dar click en el botón de **"Crear bucket"**. 

![Create bucket S3](./readme/create_s3.png)

En la siguiente pantalla, se debe ingresar el **nombre del bucket** y la **región** donde se va a crear el bucket.

![General configuration](./readme/general_configuration.png)

En este caso no vamos a configurar ninguna otra opción por el moemnto, por lo que se puede dar click en el botón de **"Crear bucket"** hasta el final del proceso.

### Configuración de permisos

Para poder subir archivos a S3, es necesario configurar los permisos del bucket. Para esto, se debe ingresar a la sección de **"Permisos"** del bucket creado. 

![Permissions](./readme/bucket_permissions.png)

En esta sección, tendremos que configurar la configuración **CORS** del bucket. Para esto, nos dirigimos a la sección de **Uso compartido de recursos entre orígenes (CORS)** y agregamos la siguiente configuración al seleccionar el botón de **"Editar"**.

```json
[
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "PUT"
        ],
        "AllowedOrigins": [
            "*"
        ],
        "ExposeHeaders": []
    }
]
```

Una vez configurado, se debe dar click en el botón de **"Guardar cambios"**, teniendo un resultado similar al siguiente.

![CORS configuration](./readme/cors_configuration.png)

## Configuración de la clave de acceso

Para poder subir archivos a S3, es necesario configurar una clave de acceso. Para esto, hacemos click en nuestro nombre de usuario en la parte superior derecha de la consola de AWS y seleccionamos la opción de **"Credenciales de seguridad"**.

![Security credentials](./readme/security_credentials.png)

Nos dirigimos al apartado de **Claves de acceso** y seleccionamos la opción de **"Crear clave de acceso"**. En este caso estaremos creando una clave de acceso para el usuario **root**, por lo que no es necesario configurar ningún otro permiso. 

De esta manera creamos la clave de acceso en el botón de **"Crear clave de acceso"**. Esto nos mostrará la **id de la clave de acceso** y el **secret de la clave de acceso**, los cuales debemos guardar en un lugar seguro.

## Configuración del proyecto

Teniendo ya el bucket de S3 configurado, se debe configurar el proyecto para poder subir archivos a este. Para esto, solo tendremos que ingresar a nuestro directorio del proyect **src/environments** y modificar el archivo **environment.ts**. En este archivo, tendremos que configurar la **id de la clave de acceso**, el **secret de la clave de acceso**, el **nombre del bucket** y la **región** donde se encuentra el bucket.

```typescript
export const environment = {
  production: false,
  aws: {
    aws_access_key_id: 'YOUR_ACCESS_KEY',
    aws_secret_access_key: 'YOUR_SECRET_KEY',
    aws_default_region: 'YOUR_REGION',
    aws_bucket_name: 'YOUR_BUCKET_NAME'
  }
};
```

Como se mostró anteriormente en la sección de **Configuración de la clave de acceso**, se debe ingresar la **id de la clave de acceso** y el **secret de la clave de acceso** que se proporcionó al crear la clave de acceso. También se debe ingresar el **nombre del bucket** y la **región** donde se encuentra el bucket.

## Ejecución del proyecto

En este caso, para evitar la instalación de las **dependencias** del proyecto y de **Angular** en nuestro equipo, creamos una **imagen de Docker** con el proyecto. Para esto, nos dirigimos a la raíz del proyecto y ejecutamos el siguiente comando:

```bash
docker build -t s3-uploader .
```

Una vez creada la imagen, ejecutamos el siguiente comando para crear un contenedor con la imagen creada:

```bash 
docker run -dti --name s3-uploader -p 4200:4200 s3-uploader
```

Esto ejecutará el proyecto en el puerto **4200**. Por lo tanto, para acceder al proyecto, simplemente ingresamos a la siguiente dirección: **http://localhost:4200**.

## Resultado

Como resultado, vemos una aplicación web que nos permite subir archivos a S3.

![Upload file](./readme/upload_file.png)

Por lo que al seleccionar un archivo al dar click en el **botón con el icono de una cubeta**, se nos mostrará el archivo seleccionado y un botón para subir el archivo. Si damos click en el botón de **"Subir archivo"**, se subirá el archivo al bucket de S3.

![Upload file](./readme/upload_custom_file.png)

Por lo que si nos dirigimos al bucket de S3, veremos que el archivo se ha subido correctamente.

![Result](./readme/result.png)