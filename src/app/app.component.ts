import { Component } from '@angular/core';
import { S3, Credentials } from 'aws-sdk';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  selectedFile: File | any;
  loading: boolean = false;
  success: boolean = false;
  error: boolean = false;

  // If file is selected, set selectedFile to the file
  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }

  // If file is removed, set selectedFile to null
  removeFile() {
    this.selectedFile = null;
  }

  // Function to upload file to S3 bucket
  uploadFile() {
    // If no file is selected, return
    if(!this.selectedFile) return;

    // Set loading to true
    this.loading = true;

    // Create S3 instance
    const credentials = new Credentials({
      accessKeyId: environment.aws.aws_access_key_id,
      secretAccessKey: environment.aws.aws_secret_access_key
    });
    const s3 = new S3({
      region: environment.aws.aws_default_region,
      credentials: credentials
    });
    const params = {
      Bucket: environment.aws.aws_bucket_name,
      Key: this.selectedFile.name,
      Body: this.selectedFile
    };

    // Upload file to S3 bucket
    s3.upload(params, (err: any, data: any) => {
      if (err) {
        console.log(err);
        this.error = true;
      } else {
        this.success = true;
      }
      setTimeout(() => {
        this.success = false;
        this.error = false;
        this.loading = false;
        this.selectedFile = null;
      }, 3000);
    });
  }
}
